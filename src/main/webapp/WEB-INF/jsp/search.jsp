<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>QuickTube</title>


</head>
<body>

	<form:form method="GET" action="/searchAjax" modelAttribute="request"
		id="search-form">
		<table>
			<caption>QuickTube Search Program</caption>
			<tr>
				<td><form:label path="searchQuery">Searching for:</form:label></td>
				<td><form:input path="searchQuery" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Show goals!" /></td>
			</tr>
		</table>
	</form:form>

	<div id="result">${title}</div>
	<!--Run through on searchResultList elements, put into the correct place with bootstrap-->
	<c:forEach items="${searchResultList}" var="result">
		<div class="row">
			<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
					<img
						src="${result.getSnippet().getThumbnails().getDefault().getUrl()}"
						alt="...">
					<div class="caption">
						<h3>${result.getSnippet().getTitle()}</h3>
						<p>${result.getSnippet().getDescription()}</p>
						<p>
						
							<a href="www.youtube.com/watch?v=${result.getId().getVideoId()}"
								class="btn btn-primary" role="button" target="_blank">Start Video</a>
						</p>
					</div>
				</div>
			</div>
		</div>


	</c:forEach>
</body>
</html>