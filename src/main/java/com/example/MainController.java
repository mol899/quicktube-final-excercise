package com.example;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;

@Controller
public class MainController {

	private static YouTube youtube;
	private static final long NUMBER_OF_VIDEOS_RETURNED = 10;

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ModelAndView helloWorld() {

	return new ModelAndView("search", "request", new Request());

	}

	@RequestMapping("/searchAjax")
	public String main(@ModelAttribute("request") Request request, Model model) {
		try {
			// This object is used to make YouTube Data API requests.
			youtube = new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(), new HttpRequestInitializer() {

	public void initialize(HttpRequest request) throws IOException {
				}
			}).setApplicationName("youtube-cmdline-search-sample").build();

			// Prompt the user to enter a query term.
			String queryTerm = request.getSearchQuery();

			// Define the API request for retrieving search results.
			YouTube.Search.List search = youtube.search().list("id,snippet");

			// Set your developer key from the {{ Google Cloud Console }} for
			// non-authenticated requests. 
			String apiKey = "AIzaSyCiD9VnACKVvpYRTUOuT7_W-2JBLrjnQFw";
			search.setKey(apiKey);
			search.setQ(queryTerm);

			// Restrict the search results to only include videos. 
			search.setType("video");

			// Set fields
			search.setFields("items(id/kind,id/videoId,snippet/title,snippet/thumbnails/default/url)");
			search.setMaxResults(NUMBER_OF_VIDEOS_RETURNED);

			// Call the API and print results in List.
			SearchListResponse searchResponse = search.execute();
			List<SearchResult> searchResultList = searchResponse.getItems();
			if (searchResultList != null) {

				model.addAttribute("title", searchResultList.get(0).getSnippet().getTitle());
				model.addAttribute("searchResultList", searchResultList);
			}
		} catch (GoogleJsonResponseException e) {
			System.err.println(
					"There was a service error: " + e.getDetails().getCode() + " : " + e.getDetails().getMessage());
		} catch (IOException e) {
			System.err.println("There was an IO error: " + e.getCause() + " : " + e.getMessage());
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return "search";
	}

}
